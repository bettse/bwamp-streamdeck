# Stream Deck Plugin for Bwamp

Trigger https://bwamp.me/ using your Stream Deck

<img width="200" alt="Screenshot" src="./screenshot.png" style="display: block; margin: 0 auto;"/>

### Setup

- Add your first Bwamp action to your Stream Deck
- Click the Auth button to launch an incognito webview of bwamp. It will automatically close when the auth(`u`) cookie has been received.
- Select the bwamp for that action.

### Notes

- The cookie is stored ‘global’ to the plugin (not specific to the action), so you only need to do the auth once and then you can add as many bwamp actions as you’d like.
- There is one shared websocket connection for all actions

### Limitations

- No room support for yet, the connection is always to the root domain
- There isn’t any indication what sounds are being played by others.
