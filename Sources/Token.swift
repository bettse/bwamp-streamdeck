//
//  Token.swift
//  Plugin
//
//  Created by Eric Betts on 11/4/21.
//  Copyright © 2021 Eric Betts. All rights reserved.
//

import Foundation
class Token {
    let rawToken : String
    let domain : String

    init(_ token : String) {
        self.rawToken = token
        //print("token: \(token)")
        let decode = token.removingPercentEncoding
        //print("decode: \(decode)")
        let parts = decode?.split(separator: ",")
        let jwt = parts?.last
        guard let jwt = jwt else {
            self.domain = ""
            return
        }
        //print("jwt: \(jwt)")
        let json = Data(base64Encoded: String(jwt))
        guard let json = json else {
            self.domain = ""
            return
        }
        //print("json: \(json)")
        do {
            let obj = try JSONSerialization.jsonObject(with: json, options: [])
            guard let obj = obj as? Dictionary<String, Any> else {
                print("obj could not be cast to Dictionary")
                self.domain = ""
                return
            }
            //print("obj \(obj)")
            guard let email = obj["Email"] as? String else {
                print("Email didn't exist or wasn't a string")
                self.domain = ""
                return
            }
            //print("email: \(email)")
            let email_parts = email.split(separator: "@")
            let domain = email_parts.last!
            //print("domain: \(domain)")
            self.domain = String(domain)
        } catch {
            print("error")
            self.domain = ""
        }
    }
}
