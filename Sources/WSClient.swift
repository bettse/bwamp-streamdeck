//
//  WSClient.swift
//  Plugin
//
//  Created by Eric Betts on 11/2/21.
//  Copyright © 2021 Eric Betts. All rights reserved.
//

import Foundation
import SwiftSoup

//{"who":"bettse","what":2,"when":"2021-11-05T02:58:57.252729129Z","sound":"💪","me":true,"cooldown":0}
struct Bwamp: Codable {
    enum What: Int, Codable {
        //case swift, combine, debugging, xcode
        case JOIN = 0, LEAVE = 1, SOUND = 2, SUPPRESSED = 3
    }
    var who: String?
    var what: What?
    var when: String?
    var sound: String?
    var me: Bool = false
    var cooldown: Int = 0
}

class WSClient : NSObject, SRWebSocketDelegate {
    var version : String = "e84c9d70e8cfd2658442a728ee0f025e10ace094"
    var csrf : String = "I80hhRhXsdKJTYCid4rE"
    var ws : SRWebSocket?
    var delegate : Plugin?
    var connectionManager: ESDConnectionManager?
    var lastToken : Token?

    override init() {
        super.init()
        getVersion()
    }

    public func setConnectionManager(_ connectionManager: ESDConnectionManager) {
        self.connectionManager = connectionManager
    }

    func connect(token: Token) {
        connect(token: token, room: token.domain)
    }

    func connect(token: Token, room: String) {
        guard ws?.readyState != .OPEN else {
            return
        }
        lastToken = token

        let myURL = URL(string:"wss://bwamp.me/stream/\(room)/?v=\(version)&csrf=\(csrf)")!
        var request = URLRequest(url: myURL)
        request.setValue("https://bwamp.me", forHTTPHeaderField: "Origin")
        ws = SRWebSocket(urlRequest: request)
        guard let ws = ws else {
            return
        }

        ws.delegate = self
        ws.requestCookies = getCookies(token.rawToken)
        ws.open()
    }

    func keepalive() {
        guard let ws = ws else {
            return
        }
        if (ws.readyState == .OPEN) {
            let play = Bwamp(what: .SUPPRESSED)
            let jsonEncoder = JSONEncoder()
            do {
                let json = try jsonEncoder.encode(play)
                try ws.send(data: json)
            } catch {
                print("Failed to send keepalive")
            }
        }

        DispatchQueue.main.asyncAfter(deadline: .now() + 5 * 60) {
            self.keepalive();
        }
    }

    func send(_ sound: String, context: Any) {
        guard let ws = ws else {
            return
        }
        if (ws.readyState == .OPEN) {
            let play = Bwamp(what: .SOUND, sound: sound)
            let jsonEncoder = JSONEncoder()
            do {
                let json = try jsonEncoder.encode(play)
                try ws.send(data: json)
                connectionManager?.showOK(forContext: context)
            } catch {
                print("Failed to encode/send \(sound)")
            }
        }
    }

    func getCookies(_ token: String) -> [HTTPCookie] {
        let consent = HTTPCookie(properties: [
            .path: "/",
            .name: "CookieConsent",
            .value: "1",
            .domain: "bwamp.me",
            .expires: "2099-10-31 03:17:54 +0000",
            .secure: "TRUE",
            .version: "1",
        ])

        let csrf = HTTPCookie(properties: [
            .path: "/",
            .name: "csrf",
            .value: self.csrf,
            .domain: "bwamp.me",
            .expires: "2099-10-31 03:17:54 +0000",
            .secure: "TRUE",
            .version: "1",
        ])

        let u = HTTPCookie(properties: [
            .path: "/",
            .name: "u",
            .value: token,
            .domain: "bwamp.me",
            .expires: "2099-10-31 03:17:54 +0000",
            .secure: "TRUE",
            .version: "1",
        ])
        return [consent!, csrf!, u!]
    }

    func getVersion() {
        let myURL = URL(string:"https://bwamp.me")!

        let task = URLSession.shared.dataTask(with: myURL) {(data, response, error) in
            guard let data = data else { return }
            let html = String(data: data, encoding: .utf8)!

            do {
                let doc: Document = try SwiftSoup.parse(html)
                let versionEl: Element = try doc.select("meta[name=version]").first()!
                let csrfEl: Element = try doc.select("meta[name=csrf]").first()!

                self.version = try versionEl.attr("content");
                self.csrf = try csrfEl.attr("content");
            } catch Exception.Error(_, let message) {
                print(message)
            } catch {
                print("error")
            }
        }

        task.resume()
    }

    func webSocket(_ webSocket: SRWebSocket, didReceiveMessageWith string: String) {
        connectionManager?.logMessage("didReceiveMessageWith \(string)")
        let jsonDecoder = JSONDecoder()
        do {
            let data = string.data(using: .utf8)
            let bwamp = try jsonDecoder.decode(Bwamp.self, from: data!)
            connectionManager?.logMessage("bwamp \(bwamp)")
            if (!bwamp.me && bwamp.what == .SOUND) {//
                // TODO: figure out good UX to show other people's bwamps
            }
        } catch {
            connectionManager?.logMessage("didReceiveMessageWith error \(error)")
        }
    }

    func webSocket(_ webSocket: SRWebSocket, didFailWithError error: Error) {
        connectionManager?.logMessage("didFailWithError \(error)")
    }
    func webSocketDidOpen(_ webSocket: SRWebSocket) {
        connectionManager?.logMessage("webSocketDidOpen")
    }
    func webSocket(_ webSocket: SRWebSocket, didCloseWithCode code: Int, reason: String?, wasClean: Bool) {
        connectionManager?.logMessage("didCloseWithCode \(code) \(reason ?? "")")
        guard let lastToken = lastToken else {
            return
        }
        connect(token: lastToken)
    }
}
