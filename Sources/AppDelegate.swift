//
//  AppDelegate.swift
//  Plugin
//
//  Created by Eric Betts on 11/2/21.
//  Copyright © 2021 Eric Betts. All rights reserved.
//

import Foundation
import AppKit

let app = NSApplication.shared

class AppDelegate: NSObject, NSApplicationDelegate {
    var plugin : Plugin?
    var connectionManager : ESDConnectionManager?

    @objc convenience init(port: NSInteger, pluginUUID: String, registerEvent: String, info: String) {
        self.init()
        plugin = Plugin(pluginUUID)
        connectionManager = ESDConnectionManager(port: Int32(port), andPluginUUID: pluginUUID, andRegisterEvent: registerEvent, andInfo: info, andDelegate: plugin!)
    }

    func applicationDidFinishLaunching(_ notification: Notification) {
        app.setActivationPolicy(.accessory)
    }
}
