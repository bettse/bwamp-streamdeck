//
//  AppDelegate.swift
//  Plugin
//
//  Created by Eric Betts on 10/30/21.
//  Copyright © 2021 Eric Betts. All rights reserved.
//

import Foundation
import AppKit
import WebKit

class Auth : NSObject, WKUIDelegate, WKNavigationDelegate {
    let window = NSWindow(contentRect: NSRect.init(origin: .zero, size: .init(width: 800, height: 600)),
                          styleMask: [.titled, .closable, .miniaturizable, .resizable],
                          backing: .buffered,
                          defer: false,
                          screen: nil)
    var connectionManager: ESDConnectionManager?
    var webView: WKWebView!
    var pluginUUID : String = ""

    init(_ pluginUUID: String) {
        super.init()
        self.pluginUUID = pluginUUID

        let myURL = URL(string:"https://bwamp.me")
        let myRequest = URLRequest(url: myURL!)
        let configuration = WKWebViewConfiguration()
        let websiteDataStore = WKWebsiteDataStore.nonPersistent()
        configuration.websiteDataStore = websiteDataStore

        let frame = window.contentView!.frame
        webView = WKWebView(frame: frame, configuration: configuration)
        webView.autoresizingMask = [.width, .height]
        webView.customUserAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 12_0_1) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/15.0 Safari/605.1.15"
        webView.navigationDelegate = self
        webView.uiDelegate = self
        webView.addObserver(self, forKeyPath: "URL", options: [.new, .old], context: nil)
        webView.load(myRequest)

        window.contentView?.addSubview(webView)
        window.center()
        window.isReleasedWhenClosed = false

        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            // For development
            //self.show("")
        }
    }

    public func setConnectionManager(_ connectionManager: ESDConnectionManager) {
        self.connectionManager = connectionManager
    }

    func show() {
        connectionManager?.logMessage("show")
        window.makeKeyAndOrderFront(self)
    }

    func saveToken(_ token : String) {
        guard let connectionManager = connectionManager else {
            print("Cannot save token without connectionManager")
            return
        }

        let settings = ["token": token]
        connectionManager.logMessage("saveToken \(token)")
        connectionManager.setGlobalSettings(settings, forContext: pluginUUID)
        connectionManager.getGlobalSettings(forContext: pluginUUID)
    }

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        guard let change = change else {
            return
        }
        if let newValue = change[.newKey] as? Int, let oldValue = change[.oldKey] as? Int, newValue != oldValue {
            print("NEW \(newValue)")
        } else {
            print("OLD \(change[.oldKey] ?? "missing")")
        }
        if (!window.isVisible) {
            return
        }
        if #available(macOS 10.13, *) {
            webView.configuration.websiteDataStore.httpCookieStore.getAllCookies { cookies in
                for cookie in cookies {
                    //print("\(cookie.name): \(cookie.value)")
                    if (cookie.name.caseInsensitiveCompare("u") == .orderedSame) {
                        DispatchQueue.main.async {
                            print("got token, closing")
                            self.saveToken(cookie.value);
                            self.window.close()
                        }
                    }
                }
            }
        } else {
            exit(0)
        }
    }
}
