//
//  Plugin.swift
//  A plug-in for Stream Deck
//
//  Created by Jarno Le Conté on 19/10/2019.
//  Copyright © 2019 Jarno Le Conté. All rights reserved.
//

import Foundation

public class Plugin: NSObject, ESDEventsProtocol {
    var connectionManager: ESDConnectionManager?
    var knownContexts: [Any] = []
    var pluginUUID : String = ""
    var globalSettings : [String:String] = [:]
    var auth : Auth?
    let ws = WSClient()

    init(_ pluginUUID: String) {
        super.init()
        self.pluginUUID = pluginUUID
        auth = Auth(pluginUUID)
        ws.delegate = self
    }

    public func setConnectionManager(_ connectionManager: ESDConnectionManager) {
        self.connectionManager = connectionManager
        auth?.setConnectionManager(connectionManager)
        ws.setConnectionManager(connectionManager)
    }
    
    public func keyDown(forAction action: String, withContext context: Any, withPayload payload: [AnyHashable : Any], forDevice deviceID: String) {
        connectionManager?.logMessage("keyDown \(payload)")
        guard let settings = payload["settings"] as? [String:String] else {
            connectionManager?.showAlert(forContext: context)
            return
        }
        guard let sound = settings["sound"] else {
            connectionManager?.showAlert(forContext: context)
            return
        }
        ws.send(sound, context: context)
    }
    
    public func willAppear(forAction action: String, withContext context: Any, withPayload payload: [AnyHashable : Any], forDevice deviceID: String) {
        connectionManager?.logMessage("willAppear \(context) \(payload)")
        // Add the context to the list of known contexts
        knownContexts.append(context)
    }
    
    public func willDisappear(forAction action: String, withContext context: Any, withPayload payload: [AnyHashable : Any], forDevice deviceID: String) {
        // Remove the context from the list of known contexts
        connectionManager?.logMessage("willDisappear \(context) \(payload)")
        knownContexts.removeAll { isEqualContext($0, context) }
    }
    
    public func propertyInspectorDidAppear(forAction action: String, withContext context: Any, withPayload payload: [AnyHashable : Any], forDevice deviceID: String) {
        connectionManager?.logMessage("propertyInspectorDidAppear")
        // Add the context to the list of known contexts
        knownContexts.append(context);

        guard let ws = ws.ws else {
            return
        }
        var data = [ "command": "updateWsState", "connection" : "unknown" ] as [String : String]
        switch ws.readyState {
        case .CONNECTING:
            data["connection"] = "Connecting"
        case .OPEN:
            data["connection"] = "Connected"
        case .CLOSED:
            data["connection"] = "Not Connected"
        case .CLOSING:
            data["connection"] = "Disconnecting"
        default:
            data["connection"] = "Unknown"
        }
        connectionManager?.logMessage("send toPropertyInspector \(data)")
        connectionManager?.send(toPropertyInspector: data, forContext: context)
    }

    public func propertyInspectorDidDisappear(forAction action: String, withContext context: Any, withPayload payload: [AnyHashable : Any], forDevice deviceID: String) {
        connectionManager?.logMessage("propertyInspectorDidDisappear \(context) \(payload)")
        // Add the context to the list of known contexts
        knownContexts.append(context);
    }

    public func sendToPlugin(forAction action: String, withContext context: Any, withPayload payload: [AnyHashable : Any], forDevice deviceID: String) {
        let command = payload["command"] as? String;
        connectionManager?.logMessage("sendToPlugin: \(command ?? "")")

        if (command == "auth") {
            self.auth?.show()
        }
    }

    public func deviceDidConnect(_ deviceID: String, withDeviceInfo deviceInfo: [AnyHashable : Any]) {
        connectionManager?.logMessage("deviceDidConnect")
        // When the plugin launches, or when the computer wakes from sleep/screensaver
        connectionManager?.getGlobalSettings(forContext: pluginUUID)
    }

    public func deviceDidDisconnect(_ deviceID: String) {
        connectionManager?.logMessage("deviceDidDisconnect")
        // Computer went to sleep/screensaver
    }

    public func didReceiveGlobalSettings(_ payload: [AnyHashable : Any]) {
        connectionManager?.logMessage("didReceiveGlobalSettings \(payload)")
        guard let settings = payload["settings"] as? [String:String] else {
            return
        }
        self.globalSettings = settings
        guard let rawToken = settings["token"] else {
            return
        }
        let token = Token(rawToken)
        ws.connect(token: token)
    }
}
